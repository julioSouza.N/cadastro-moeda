package br.com.projeto.cadastro.moeda.fabrica;

import br.com.projeto.cadastro.moeda.enumeracao.Enumeracao;
import br.com.projeto.cadastro.moeda.modelo.Moeda;

public interface FabricaTeste {
	
	public static Moeda fabricarMoeda() {
		

		Moeda moeda = new Moeda();
		
		moeda.setValor(3.78f);
		moeda.setNome("dolar");
		moeda.setCodigo_transf(Enumeracao.REAL);
		
		return moeda;
		
	}
	
	

}
