package br.com.projeto.cadastro.moeda.servico;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.projeto.cadastro.moeda.excessao.MoedaJahCadastradaException;
import br.com.projeto.cadastro.moeda.fabrica.FabricaTeste;
import br.com.projeto.cadastro.moeda.modelo.Moeda;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CadastroTesteServico {

	@Autowired
	private Servico servico;

	@Test
	public void cadastrarMoedaEmServico_1_Test() throws MoedaJahCadastradaException {

		Moeda moeda = FabricaTeste.fabricarMoeda();
		Moeda moedaCadastrada = servico.cadastraMoeda(moeda);

		Assert.assertEquals(moeda, moedaCadastrada);
		Assert.assertTrue(moedaCadastrada.getCodigo() > 0);

	}

	@Test(expected = MoedaJahCadastradaException.class)
	public void cadastrarMoedaEmServico_2_Test() throws MoedaJahCadastradaException {
		Moeda moeda = FabricaTeste.fabricarMoeda();
		servico.cadastraMoeda(moeda);
	}

}
