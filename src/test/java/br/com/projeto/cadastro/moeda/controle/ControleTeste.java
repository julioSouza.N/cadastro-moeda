package br.com.projeto.cadastro.moeda.controle;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.projeto.cadastro.moeda.fabrica.FabricaTeste;
import br.com.projeto.cadastro.moeda.modelo.Moeda;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ControleTeste {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void cadastroEViolacaoMoeda() {

		Moeda moeda = FabricaTeste.fabricarMoeda();

		System.out.println(construtorURLCadastarMoeda());
	
		ResponseEntity<Moeda> moedaCadastar = restTemplate.postForEntity(construtorURLCadastarMoeda(), moeda,
				Moeda.class);
		
		Assert.assertEquals("Status precisa ser Ok", HttpStatus.OK, moedaCadastar.getStatusCode());
		
		Moeda moedaCadastrada = moedaCadastar.getBody();
		
		Assert.assertTrue(Float.compare(moeda.getValor(), moedaCadastrada.getValor()) == 0);
		
		Assert.assertEquals(moeda.getCodigo_transf(), moedaCadastrada.getCodigo_transf());
		
		
		Assert.assertEquals(moeda.getNome(), moedaCadastrada.getNome());
		

	}

	private String construtorURLCadastarMoeda() {
		return construtorURL() + "/api/cadastrar";
	}

	private String construtorURL() {
		return "http://localhost:" + port;
	}

}
