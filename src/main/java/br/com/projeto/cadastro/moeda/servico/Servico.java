package br.com.projeto.cadastro.moeda.servico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projeto.cadastro.moeda.enumeracao.Enumeracao;
import br.com.projeto.cadastro.moeda.excessao.MoedaJahCadastradaException;
import br.com.projeto.cadastro.moeda.modelo.Moeda;
import br.com.projeto.cadastro.moeda.repositorio.Repositorio;

@Service
public class Servico {

	Logger logger = LoggerFactory.getLogger(Servico.class);

	@Autowired
	private Repositorio repositorio;

	public Moeda cadastraMoeda(Moeda moeda) throws MoedaJahCadastradaException {

		logger.info("servico: cadastrando moeda");
		logger.info("servico: verificando se moeda ja foi cadastrada");

		if (moedaJahFoiCadastrada(moeda)) {
			logger.info("servico: moeda ja foi cadastrada");
			throw new MoedaJahCadastradaException();
		} else {
			logger.info("servico: moeda cadastrada");
			return repositorio.save(moeda);
		}

	}

	private boolean moedaJahFoiCadastrada(Moeda moeda) {
		
		Enumeracao enumeracao = moeda.getCodigo_transf();
		String nomeMoeda = moeda.getNome();
		
		logger.info("codigoTrans : {} nomeMoeda:{}", enumeracao, nomeMoeda);
		
		Long codigo = repositorio.existeMoeda(enumeracao, nomeMoeda);
		
		return codigo != null;

	}
}
