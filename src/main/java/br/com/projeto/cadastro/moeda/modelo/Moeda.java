package br.com.projeto.cadastro.moeda.modelo;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.projeto.cadastro.moeda.enumeracao.Enumeracao;

@Entity
@Table(name = "Moedas")

public class Moeda {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private long codigo;

	private String nome;
	private float valor;
	
	@Enumerated(EnumType.STRING)
	private Enumeracao codigo_transf;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public Enumeracao getCodigo_transf() {
		return codigo_transf;
	}

	public void setCodigo_transf(Enumeracao real) {
		this.codigo_transf = real;
	}

	public long getCodigo() {
		return codigo;
	}

	@Override
	public String toString() {
		return "Moeda [codigo=" + codigo + ", nome=" + nome + ", valor=" + valor + ", codigo_transf=" + codigo_transf
				+ "]";
	}

}
