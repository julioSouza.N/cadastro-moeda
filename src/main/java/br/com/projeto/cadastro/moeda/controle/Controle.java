package br.com.projeto.cadastro.moeda.controle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import br.com.projeto.cadastro.moeda.excessao.MoedaJahCadastradaException;
import br.com.projeto.cadastro.moeda.modelo.Moeda;
import br.com.projeto.cadastro.moeda.servico.Servico;

@RestController
public class Controle {

	Logger logger = LoggerFactory.getLogger(Controle.class);

	@Autowired
	private Servico servico;

	@GetMapping("/")
	public ModelAndView listarMoeda() {

		logger.info("Requisicao para listar moedas cadastradas : ");

		return new ModelAndView("index");

	}

	@PostMapping("/api/cadastrar")
	public Moeda cadastarMoeda(@RequestBody Moeda moeda) throws MoedaJahCadastradaException {

		logger.info("cadastrando moeda:");

		logger.info(moeda.toString());

		return servico.cadastraMoeda(moeda);
	}

}
