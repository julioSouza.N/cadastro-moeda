package br.com.projeto.cadastro.moeda.excessao;

public class MoedaJahCadastradaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8722849716431096563L;

	public MoedaJahCadastradaException() {
		super();
	}

	@Override
	public String getMessage() {
		return "MOEDA JAH CADASTRADA";
	}

}
