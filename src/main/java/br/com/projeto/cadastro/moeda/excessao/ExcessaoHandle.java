package br.com.projeto.cadastro.moeda.excessao;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExcessaoHandle extends ResponseEntityExceptionHandler {

	/**
	 * 
	 * @param ex
	 * @return response
	 */
	@ExceptionHandler({ MoedaJahCadastradaException.class })
	public ResponseEntity<Object> handleMoedaJahCadastrada(Exception ex) {
		return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
	}

}
