package br.com.projeto.cadastro.moeda.enumeracao;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum Enumeracao implements Serializable {

	REAL("REAL");

	private String codigo;

	private Enumeracao(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}


}
