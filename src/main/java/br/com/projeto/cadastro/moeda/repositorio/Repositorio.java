package br.com.projeto.cadastro.moeda.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.projeto.cadastro.moeda.enumeracao.Enumeracao;
import br.com.projeto.cadastro.moeda.modelo.Moeda;

public interface Repositorio extends JpaRepository<Moeda, Long> {

	@Query(value = "select codigo from Moeda m where m.codigo_transf = ?1 and m.nome = ?2 ")
	Long existeMoeda(Enumeracao enumeracao, String nomeMoeda);
}
